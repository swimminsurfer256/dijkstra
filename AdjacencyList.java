import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class AdjacencyList {
	private HashMap<Node, ArrayList<Edge>> masterMap = new HashMap<Node, ArrayList<Edge>>();
	
	public void addEdge(Node from, Node to, int distance) {
	       ArrayList<Edge> list;
	       if(!masterMap.containsKey(from)) {
	           list = new ArrayList<Edge>();
	           masterMap.put(from, list);
	       } else {
	           list = masterMap.get(from);
	       }
	       list.add(new Edge(from, to, distance));
	       Collections.sort(list);
	   }
	
	public ArrayList<Edge> getNeighbors(Node from) {
		return masterMap.get(from);
	}

	public Set<Node> getKeys() {
		return masterMap.keySet();
	}
	
	@Override
	public String toString() {
		String overallString = "";
		for (Node node : masterMap.keySet()) {
			overallString += "Node: " + node.id + "\n";
			overallString += "Edges: ";
			ArrayList<Edge> list;
			for (int i = 0; i < (list = masterMap.get(node)).size(); i++) {
				overallString += list.get(i).distance + "// ";
			}
			overallString += "\n\n";
		}
		return overallString;
	}
}
