
public class Node implements Comparable<Node> {
	final int id;
	int finalDistance;
	
	public Node(final int id) {
		this.id = id;
	}
	
	@Override
	public int compareTo(Node o) {
		if (this.id == o.id) {
			return 0;
		} else if (this.id < o.id) {
			return -1;
		} else {
			return 1;
		}
	}

	@Override
	public boolean equals(Object obj) {
		return this.id == ((Node)obj).id;
	}

	@Override
	public int hashCode() {
		return this.id;
	}
}
