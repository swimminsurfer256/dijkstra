
public class Edge implements Comparable<Edge>{
	final Node to, from;
	final int distance;
	
	public Edge(final Node argFrom, final Node argTo, final int d) {
	       from = argFrom;
	       to = argTo;
	       distance = d;
	   }
	
	@Override
	public int compareTo(Edge o) {
		return this.distance - o.distance;
	}
}
