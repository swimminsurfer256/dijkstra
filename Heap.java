import java.util.ArrayList;

public class Heap {

	private ArrayList<HeapNode> nodeList;

	class HeapNode {
		public int distance;
		public int vertexID;

		public HeapNode(int vertexID, int distance) {
			this.vertexID = vertexID;
			this.distance = distance;
		}

		public int parentIndex() {
			return (nodeList.indexOf(this) - 1)/2;
		}

		public int leftChildIndex() {
			return 2 * nodeList.indexOf(this) + 1;
		}

		public int rightChildIndex() {
			return 2 * nodeList.indexOf(this) + 2;
		}

		@Override
		public boolean equals(Object o) {
			if(!(o instanceof HeapNode)) return false;
			HeapNode node = (HeapNode)o;
			if(this.vertexID == node.vertexID) return true;
			else return false;
		}

		@Override
		public int hashCode() {
			return vertexID;
		}
	}

	public Heap(int startID) {
		HeapNode node = new HeapNode(startID, 0);
		nodeList = new ArrayList<HeapNode>();
		nodeList.add(node);
	}

	public void insert(int vertexID, int distance) {
		HeapNode node = new HeapNode(vertexID, distance);
		int index = nodeList.indexOf(node);
		if(index != -1) {
			nodeList.get(index).distance = distance;
			siftUp(nodeList.get(index));
		} else {
			nodeList.add(node);
			siftUp(node);
		}
	}

	public boolean isEmpty() {
		return nodeList.isEmpty();
	}

	public int getMinID() {
		if(!this.isEmpty())
			return nodeList.get(0).vertexID;
		else
			return -1;
	}

	public boolean contains(int vertexID) {
		HeapNode node = new HeapNode(vertexID,0);
		return nodeList.contains(node);
	}

	public int removeMin() {
		if(nodeList.size() > 1) {
			HeapNode min = nodeList.remove(0);
			HeapNode last = nodeList.remove(nodeList.size() - 1);
			nodeList.add(0, last);
			siftDown(last);
			return min.distance;
		} else {
			return nodeList.remove(0).distance;
		}
	}

	public int getDistance(int vertexID) {
		HeapNode node = new HeapNode(vertexID, 0);
		int index = nodeList.indexOf(node);
		return nodeList.get(index).distance;
	}

	private void siftDown(HeapNode node) {
		while(true) {
			int rightChildIndex = node.rightChildIndex();
			int leftChildIndex = node.leftChildIndex();
			int minIndex = 0;
			if(rightChildIndex >= nodeList.size()) {
				if(leftChildIndex >= nodeList.size())
					break;
				else
					minIndex = leftChildIndex;
			} else {
				if(nodeList.get(leftChildIndex).distance <= nodeList.get(rightChildIndex).distance)
					minIndex = leftChildIndex;
				else 
					minIndex = rightChildIndex;
			}
			if(node.distance > nodeList.get(minIndex).distance) {
				HeapNode child = nodeList.remove(minIndex);
				int nodeIndex = nodeList.indexOf(node);
				nodeList.remove(node);
				nodeList.add(nodeIndex, child);
				nodeList.add(minIndex, node);
			} else {
				break;
			}
		}
	}

	private void siftUp(HeapNode node) {
		int parentIndex = node.parentIndex();
		while(parentIndex >= 0) {
			HeapNode parent = nodeList.get(parentIndex);
			if(parent.distance > node.distance) {
				nodeList.remove(parent);
				int nodeIndex = nodeList.indexOf(node);
				nodeList.remove(node);
				nodeList.add(parentIndex, node);
				nodeList.add(nodeIndex, parent);
				parentIndex = node.parentIndex();
			} else {
				break;
			}
		}
	}

}
