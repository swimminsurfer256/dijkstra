import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Dijkstra {
	static AdjacencyList masterGraph = new AdjacencyList();
	
	public static void main(String[] args) throws IOException {
		FileInputStream stream = new FileInputStream("src/us.gr"); // USE us.gr to test, NY.gr for actual
		InputStreamReader reader = new InputStreamReader(stream);
		Scanner scanner = new Scanner(reader);
		
		int n = -1;
		int arcs = -1;
		boolean keepGoing = true;
		while (scanner.hasNext() && keepGoing) {
			String line = (String) scanner.nextLine();
			if (line.indexOf("p") == 0) {
				line = line.replaceAll("p\\s+sp\\s+", "");
				String[] contents = line.split("\\s+");
				n = Integer.parseInt(contents[0]);
				arcs = Integer.parseInt(contents[1]);
				keepGoing = false;
			}
		}
		System.out.println("We got " + n + " nodes and " + arcs + " arcs");
		if (n == -1 || arcs == -1) {
			throw new Error("Read error");
		}
		
		while (scanner.hasNext()) {
			String line = scanner.nextLine();
			
			if (line.indexOf("a") == 0) {
				// We must use the data in this line
				line = line.replaceAll("a\\s+", "");
				String[] contents = line.split("\\s+");
				
				int nodeOne = Integer.parseInt(contents[0]);
				Node from = new Node(nodeOne);
				int nodeTwo = Integer.parseInt(contents[1]);
				Node to = new Node(nodeTwo);
				int distance = Integer.parseInt(contents[2]);
				
				masterGraph.addEdge(from, to, distance);
			}
		}
		scanner.close();
		/*
		 MasterGraph should now be an adjacency list of all the Node:List<Edges> pairs in the input file
		*/
		
		int fuckCount = 0;
		
		for (int i = 0; i < 100; i++) {
			Set<Node> nodeSet = masterGraph.getKeys();
			Object[] allNodes = nodeSet.toArray();
			Random r = new Random();
			Node start = (Node) allNodes[r.nextInt(allNodes.length)];
			Node end = (Node) allNodes[r.nextInt(allNodes.length)];
			start = (Node) allNodes[26];
			end = (Node) allNodes[34];
			int usualDijkstraDistance = usualDijkstra(start, end);
			System.out.println("Distance from " + start.id + " to " + end.id
					+ ": " + usualDijkstraDistance);
			int bothEndsDijkstraDistance = bothEndsDijkstra(start, end);
			if (usualDijkstraDistance != bothEndsDijkstraDistance) {
				System.out.println("Fuck");
				fuckCount++;
			}
			if (usualDijkstraDistance == -1) {
				System.out.println("No path found regular Dijkstra");
			}
			if (bothEndsDijkstraDistance == -1) {
				System.out.println("No path found both ends Dijkstra");
			}
			System.out.println("Distance from " + start.id + " to " + end.id
					+ ": " + bothEndsDijkstraDistance);
		}
		System.out.println("Fuck Count: " + fuckCount);
//		FileOutputStream fos = new FileOutputStream("src/out.txt"); 
//		OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8");
//		out.write(masterGraph.toString());
//		out.close();
	}
	
	private static int usualDijkstra(Node startNode, Node endNode) {
		if(startNode.id == endNode.id) return 0;
		Heap heap = new Heap(startNode.id);
		ArrayList<Node> visitedNodes = new ArrayList<Node>();
		
		boolean found = false;
		int distance = -1;
		while (!heap.isEmpty() || !found) {
			int currentID = heap.getMinID();
			Node currentNode = new Node(currentID);
			if (endNode.equals(currentNode)) {
				found = true;
				distance = heap.getDistance(currentID);
				System.out.println("Found it!");
				break;
			}
			ArrayList<Edge> neighbors = masterGraph.getNeighbors(currentNode);
			
			visitedNodes.add(currentNode);
			for (Edge edge : neighbors) {
				if (!visitedNodes.contains(new Node(edge.to.id))) { // If we haven't already visited the node...
					if (heap.contains(edge.to.id)) { // If the heap already contains our destination node...
						/* If the distance to the destination node in our heap is greater than the distance
						\* to our source node plus the edge distance we are looking at... */
						if (heap.getDistance(edge.to.id) > heap.getDistance(edge.from.id) + edge.distance) {
							// We need to change the node distance to the new, shorter distance that we found
							heap.insert(edge.to.id, heap.getDistance(edge.from.id) + edge.distance);
						}
					} else { // The heap doesn't contain our destination node and we haven't yet visited it either
						heap.insert(edge.to.id, heap.getDistance(edge.from.id) + edge.distance);
					}
				}
			}
			heap.removeMin();
		}
		
		return distance;
	}
	
	private static int bothEndsDijkstra(Node startNode, Node endNode) {
		if(startNode.id == endNode.id) return 0;
		Heap startHeap = new Heap(startNode.id);
		Heap endHeap = new Heap(endNode.id);
		ArrayList<Node> startVisitedNodes = new ArrayList<Node>();
		ArrayList<Node> endVisitedNodes = new ArrayList<Node>();
		
		boolean found = false;
		int distance = Integer.MAX_VALUE;
		while ((!startHeap.isEmpty() && !endHeap.isEmpty()) || !found) {
			int startCurrentID = startHeap.getMinID();
			int endCurrentID = endHeap.getMinID();
			Node startCurrentNode = new Node(startCurrentID);
			Node endCurrentNode = new Node(endCurrentID);
			ArrayList<Edge> startNeighbors = masterGraph.getNeighbors(startCurrentNode);
			ArrayList<Edge> endNeighbors = masterGraph.getNeighbors(endCurrentNode);
			


			for (Edge edge : startNeighbors) {
				if (!startVisitedNodes.contains(new Node(edge.to.id))) { // If we haven't already visited the node...				
					if (startHeap.contains(edge.to.id)) { // If the heap already contains our destination node...
						/* If the distance to the destination node in our heap is greater than the distance
						\* to our source node plus the edge distance we are looking at... */
						if (startHeap.getDistance(edge.to.id) > startHeap.getDistance(edge.from.id) + edge.distance) {
							// We need to change the node distance to the new, shorter distance that we found
							startHeap.insert(edge.to.id, startHeap.getDistance(edge.from.id) + edge.distance);
						}
					} else { // The heap doesn't contain our destination node and we haven't yet visited it either
						startHeap.insert(edge.to.id, startHeap.getDistance(edge.from.id) + edge.distance);
					}
					if(endVisitedNodes.contains(new Node(edge.to.id))) {
						int currentDistance = startHeap.getDistance(startCurrentID) + edge.distance + endVisitedNodes.get(endVisitedNodes.indexOf(new Node(edge.to.id))).finalDistance;
						if(currentDistance < distance) {
							distance = currentDistance;
						}
					}	
				}
			}
			startCurrentNode.finalDistance = startHeap.removeMin();
			startVisitedNodes.add(startCurrentNode);
			
			for (Edge edge : endNeighbors) {
				if (!endVisitedNodes.contains(new Node(edge.to.id))) { // If we haven't already visited the node...	
					if (endHeap.contains(edge.to.id)) { // If the heap already contains our destination node...
						/* If the distance to the destination node in our heap is greater than the distance
						\* to our source node plus the edge distance we are looking at... */
						if (endHeap.getDistance(edge.to.id) > endHeap.getDistance(edge.from.id) + edge.distance) {
							// We need to change the node distance to the new, shorter distance that we found
							endHeap.insert(edge.to.id, endHeap.getDistance(edge.from.id) + edge.distance);
						}
					} else { // The heap doesn't contain our destination node and we haven't yet visited it either
						endHeap.insert(edge.to.id, endHeap.getDistance(edge.from.id) + edge.distance);
					}
					if(startVisitedNodes.contains(new Node(edge.to.id))) {
						int currentDistance = endHeap.getDistance(endCurrentID) + edge.distance + startVisitedNodes.get(startVisitedNodes.indexOf(new Node(edge.to.id))).finalDistance;
						if(currentDistance < distance) {
							distance = currentDistance;
						}
					}
				}
			}
			endCurrentNode.finalDistance = endHeap.removeMin();
			endVisitedNodes.add(endCurrentNode);
			if(endCurrentNode.finalDistance + startCurrentNode.finalDistance >= distance) {
				found = true;
				break;
			}
		}
		
		return distance;
	}
}
